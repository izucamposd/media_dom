var lista = [], qtd_notas = 1;


function receber_nota (){
    let input_valor = document.querySelector(".input_nota");
    let texto = document.createElement("p");
    texto = input_valor.value;
    input_valor.value = "";
    if (texto == ""){
        return window.alert("Por favor, insira uma nota.");
    } 
    
    if (isNaN(texto) != false){
        return window.alert("A nota digitada é inválida, por favor, insira uma nota válida.");
    }
    texto = parseFloat (texto);
    
    if ((texto<0) || (texto>10)){
        window.alert("A nota digitada é inválida, por favor, insira uma nota válida.");
    } else {
    let escopo = document.createElement("div");
    escopo.append("A nota " + qtd_notas + " foi " + texto);
    qtd_notas++;
    let quadro = document.querySelector(".div_msg");
    quadro.append(escopo);
    lista.push(texto);
    
    }
}

function cal_media (){
    var valor_media = 0;
    for (var valor in lista){
        valor_media = (valor_media + (lista[valor]));
    }
    valor_media = valor_media / lista.length;
    valor_media = valor_media.toFixed(2);
    let escopo = document.createElement("div");
    escopo.append(valor_media);
    let quadro = document.querySelector(".div_media");
    quadro.append(escopo);
}

let btn_enviar = document.querySelector("#botao_enviar");
btn_enviar.addEventListener("click", receber_nota);

let btn_media = document.querySelector("#botao_cal_media");
btn_media.addEventListener("click", cal_media);